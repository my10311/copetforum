from CopetForum.models import AccessToken
from CopetForum.models import Response
from CopetForum.models import Subject
from CopetForum.models import Topic
from django.contrib import admin


admin.site.register(Topic)
admin.site.register(Subject)
admin.site.register(Response)
admin.site.register(AccessToken)
