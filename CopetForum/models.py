from django.contrib.auth.models import User
from django.db import models


class AccessToken(models.Model):
    class Meta:
        verbose_name = "Access Token"
        verbose_name_plural = "Access Tokens"

    pageID = models.TextField(verbose_name="頁面 ID")
    tag = models.TextField(unique=True, verbose_name="Access token 代號")
    token = models.TextField(verbose_name="Access token")
    expired = models.DateTimeField(null=True, blank=True, verbose_name="失效時間")

    def __str__(self):
        return self.tag


class Topic(models.Model):
    class Meta:
        verbose_name = "論壇分類主題"
        verbose_name_plural = "論壇分類主題"

    name = models.CharField(max_length=50, verbose_name="分類主題名稱", unique=True)
    description = models.TextField(verbose_name="描述")

    def __str__(self):
        return self.name


class Subject(models.Model):
    class Meta:
        verbose_name = "討論主題"
        verbose_name_plural = "討論主題"

    name = models.CharField(max_length=50, verbose_name="主題名稱")
    topic = models.ForeignKey(to=Topic, verbose_name="所屬主題")
    timestamp = models.DateTimeField(
        auto_now_add=True, verbose_name="時間戳記")
    author = models.ForeignKey(to=User, verbose_name="作者")

    def __str__(self):
        return self.name


class Response(models.Model):
    class Meta:
        verbose_name = "回應"
        verbose_name_plural = "回應"

    author = models.ForeignKey(to=User, verbose_name="作者")
    timestamp = models.DateTimeField(
        auto_now_add=True, verbose_name="時間戳記")
    content = models.TextField(verbose_name="回應內容")
    subject = models.ForeignKey(to=Subject, verbose_name="所屬主題")

    def __str__(self):
        return self.content
