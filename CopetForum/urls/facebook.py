from django.conf.urls import patterns, include, url
from CopetForum.settings import PAGE_NAME

urlpatterns = patterns(
    'CopetForum.views.facebook',
    url(r'^setPageToken/$', 'getAuthorize', {"pageName": PAGE_NAME}),
    url(r'^callback/$', 'callback'),
)
