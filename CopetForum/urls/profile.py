from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'CopetForum.views.profile',
    url(r'^$', 'update'),
    url(r'^success/$', 'success'),
)
