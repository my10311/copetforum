from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'django.contrib.auth.views',  # For Authentication
    (r'^login/$', 'login',
     {'template_name': 'CopetForum/authenticate/login.html'}),
    (r'^logout/$', 'logout',
     {'template_name': 'CopetForum/authenticate/logout.html'}),
    (r'^password_change/$', 'password_change',
     {'template_name': 'CopetForum/profile/changPassword.html'}),
    (r'^password_change_done/$', 'password_change_done',
     {'template_name': 'CopetForum/profile/changPasswordDone.html'}),
)
urlpatterns += patterns(
    'CopetForum.views.register',  # For Authentication
    url(r'^register/$', 'register'),
    url(r'^registerDone/$', 'registerDone'),
)
