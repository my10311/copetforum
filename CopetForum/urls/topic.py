from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'CopetForum.views.topic',
    url(r'^create/$', 'create'),
    url(r'^$', 'listAll'),
    url(r'^(?P<topicID>\d+)/$', 'listAllSubjectInTopic'),
    url(r'^(?P<topicID>\d+)/delete/$', 'delete'),
)
