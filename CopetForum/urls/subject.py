from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'CopetForum.views.subject',
    url(r'^create/topic(?P<topicID>\d+)/$', 'create'),
    url(r'^(?P<subjectID>\d+)/$', 'listAllResponseInSubject'),
    url(r'^(?P<subjectID>\d+)/delete/$', 'delete'),
    url(r'^(?P<redirectSubjectID>\d+)/delete/(?P<responseID>\d+)/$',
        'deleteResponse'),
)
