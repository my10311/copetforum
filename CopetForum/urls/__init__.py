from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()
handler403 = 'CopetForum.views.handle403'
handler404 = 'CopetForum.views.handle404'
handler500 = 'CopetForum.views.handle500'

urlpatterns = patterns(
    'CopetForum.views',
    url(r'^$', 'home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('CopetForum.urls.accounts')),
    url(r'^topic/', include('CopetForum.urls.topic')),
    url(r'^subject/', include('CopetForum.urls.subject')),
    url(r'^facebook/', include('CopetForum.urls.facebook')),
    url(r'^profile/', include('CopetForum.urls.profile')),
    url(r'^management/', 'management.management')
)
