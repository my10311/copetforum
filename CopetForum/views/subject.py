from django.contrib.auth.decorators import login_required
from CopetForum.views.facebook import syncToFacebook
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from CopetForum.models import Response
from CopetForum.models import Subject
from django.shortcuts import redirect
from django.shortcuts import render
from CopetForum.models import Topic
from django import forms


class ResponseForm(forms.ModelForm):
    class Meta:
        model = Response
        exclude = ('author', 'subject')


class Form(forms.ModelForm):
    class Meta:
        model = Subject
        exclude = ('author', 'topic')


@login_required
def create(request, topicID):
    topic = get_object_or_404(Topic, pk=topicID)
    form = Form()
    responseForm = ResponseForm()
    if request.method == "POST":
        form = Form(request.POST)
        responseForm = ResponseForm(request.POST)
        if form.is_valid() and responseForm.is_valid():
            subject = form.save(commit=False)
            subject.author = request.user
            subject.topic = topic
            subject.save()
            response = responseForm.save(commit=False)
            response.author = request.user
            response.subject = subject
            response.save()
            messagePackage = {
                "username": request.user.username,
                "first_name": request.user.first_name,
                "topic": topic.name,
                "subject": subject.name,
                "response": response.content}
            toFacebook = (
                "{username}({first_name})在{topic}的{subject}回應了：\n"
                "{response}").format(**messagePackage)
            syncToFacebook(toFacebook)
            return redirect(reverse(
                "CopetForum.views.topic.listAllSubjectInTopic",
                kwargs={"topicID": topicID}))
    return render(request, 'CopetForum/forum/subject/create.html',
                  {"form": form, "responseForm": responseForm})


@login_required
def listAllResponseInSubject(request, subjectID):
    subject = get_object_or_404(Subject, pk=subjectID)
    responseForm = ResponseForm()
    if request.method == "POST":
        responseForm = ResponseForm(request.POST)
        if responseForm.is_valid():
            response = responseForm.save(commit=False)
            response.author = request.user
            response.subject = subject
            response.save()
            messagePackage = {
                "username": request.user.username,
                "first_name": request.user.first_name,
                "topic": subject.topic.name,
                "subject": subject.name,
                "response": response.content}
            toFacebook = (
                "{username}({first_name})在{topic}的{subject}回應了：\n"
                "{response}").format(**messagePackage)
            syncToFacebook(toFacebook)
            return redirect(reverse(
                "CopetForum.views.subject.listAllResponseInSubject",
                kwargs={"subjectID": subjectID}))
    List = Response.objects.filter(subject=subjectID)
    return render(request, 'CopetForum/forum/subject/listAllResponse.html',
                  {"form": responseForm, "List": List, "subject": subject})


@login_required
def delete(request, subjectID):
    subject = get_object_or_404(Subject, id=subjectID)
    topicID = subject.topic.id
    if request.user.is_superuser or request.user == subject.author:
        subject.delete()
    return redirect(reverse(
        "CopetForum.views.topic.listAllSubjectInTopic",
        kwargs={"topicID": topicID}))


@login_required
def deleteResponse(request, redirectSubjectID, responseID):
    response = get_object_or_404(Response, id=responseID)
    subject = response.subject
    if request.user.is_superuser or request.user == response.author:
        response.delete()
        if subject.response_set.count() == 0:
            topicID = subject.id
            subject.delete()
            return redirect(reverse(
                "CopetForum.views.topic.listAllSubjectInTopic",
                kwargs={"topicID": topicID}))
    return redirect(reverse(
        "CopetForum.views.subject.listAllResponseInSubject",
        kwargs={"subjectID": redirectSubjectID}))
