from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.views import login_required
from django.views.decorators.http import require_GET
from CopetForum.settings import CLIENT_SECRET
from CopetForum.settings import REDIRECT_URL
from CopetForum.settings import CLIENT_ID
from CopetForum.models import AccessToken
from http.client import HTTPSConnection
from django.shortcuts import redirect
from django.shortcuts import render
from urllib.parse import urlencode
from urllib.parse import parse_qs
from django.http import Http404
import json


@login_required
@user_passes_test(lambda u: u.is_superuser)
def getAuthorize(request, pageName):
    parameter = urlencode({
        "client_id": CLIENT_ID,
        "redirect_uri": REDIRECT_URL,
        "scope": "publish_stream,manage_pages",
        "state": pageName})
    return redirect(
        "https://www.facebook.com/dialog/oauth?" + parameter)


@require_GET
@login_required
@user_passes_test(lambda u: u.is_superuser)
def callback(request):
    if 'code' not in request.GET:
        return render(request, "CopetForum/management/setPageTokenFail.html")
    if 'state' not in request.GET:
        return render(request, "CopetForum/management/setPageTokenFail.html")
    code = request.GET['code']
    tag = request.GET['state']
    parameter = urlencode({
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "redirect_uri": REDIRECT_URL,
        "code": code})
    connection = HTTPSConnection("graph.facebook.com")
    connection.request("GET", "/oauth/access_token?" + parameter)
    response = connection.getresponse()
    responseData = parse_qs(response.read().decode())
    accessToken = responseData['access_token'][0]
    parameter = urlencode({"access_token": accessToken})
    connection = HTTPSConnection("graph.facebook.com")
    connection.request("GET", "/me/accounts?" + parameter)
    response = connection.getresponse()
    responseData = json.loads(response.read().decode())
    accountsData = responseData['data']
    pageToken = None
    pageID = None
    for accounts in accountsData:
        if accounts['name'] == tag:
            pageToken = accounts['access_token']
            pageID = accounts['id']
    if pageToken:
        try:
            storedToken = AccessToken.objects.get(tag=tag)
        except AccessToken.DoesNotExist:
            storedToken = AccessToken(
                tag=request.GET['state'], token=pageToken)
        storedToken.tag = tag
        storedToken.token = pageToken
        storedToken.pageID = pageID
        storedToken.save()
        return render(request, "CopetForum/management/setPageToken.html")
    return render(request, "CopetForum/management/setPageTokenFail.html")


def syncToFacebook(message):
    storedTokenList = AccessToken.objects.all()
    for token in storedTokenList:
        parameter = urlencode(
            {"access_token": token.token, "message": message})
        connection = HTTPSConnection("graph.facebook.com")
        connection.request(
            "POST", "/{0}/feed".format(token.pageID), parameter)
