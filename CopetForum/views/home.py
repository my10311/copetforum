from CopetForum.models import Response
from django.shortcuts import render


def home(request):
    List = Response.objects.all().order_by('-timestamp')[:5]
    return render(request, 'CopetForum/home.html', {"List": List})
