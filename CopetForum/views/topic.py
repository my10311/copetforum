from django.shortcuts import render
from CopetForum.models import Topic
from CopetForum.models import Subject
from django import forms
from django.contrib.auth.views import login_required
from django.contrib.auth.decorators import permission_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404


@login_required
@permission_required("CopetForum.add_topic",
                     raise_exception=True)
def create(request):
    class Form(forms.ModelForm):
        class Meta:
            model = Topic

    form = Form()
    if request.method == "POST":
        form = Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("CopetForum.views.topic.listAll"))
    return render(request, 'CopetForum/forum/topic/create.html',
                  {"form": form})


def listAll(request):
    List = Topic.objects.all()
    return render(request, 'CopetForum/forum/topic/listAll.html',
                  {"List": List})


def listAllSubjectInTopic(request, topicID):
    topic = get_object_or_404(Topic, pk=topicID)
    List = Subject.objects.filter(topic=topicID).order_by('-timestamp')
    return render(request, 'CopetForum/forum/topic/listOne.html',
                  {"List": List, "topic": topic})


@login_required
def delete(request, topicID):
    topic = get_object_or_404(Topic, id=topicID)
    if request.user.is_superuser:
        topic.delete()
    return redirect(reverse("CopetForum.views.topic.listAll"))
