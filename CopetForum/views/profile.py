from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
from django import forms


@login_required
def update(request, message=None):
    class Form(forms.ModelForm):
        class Meta:
            model = User
            fields = ['first_name']

    form = Form(instance=request.user)
    if request.method == 'POST':
        form = Form(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(reverse('CopetForum.views.profile.success'))
    return render(request, 'CopetForum/profile/profile.html',
                  {"form": form, "message": message})


def success(request):
    return update(request, "修改成功！")
