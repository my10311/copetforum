from django.core.validators import RegexValidator
from django.contrib.auth import authenticate
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.shortcuts import redirect
from django.shortcuts import render
from datetime import datetime
from django import forms


def register(request):
    class Form(forms.Form):
        username = forms.CharField(
            max_length=30, validators=[RegexValidator("^\w{1,30}$")],
            help_text="請輸入最多30字的英數字或底線", label="帳號",
            required=True)
        name = forms.CharField(
            max_length=30, help_text="請輸入您的姓名", label="姓名",
            required=True)
        password = forms.CharField(
            max_length=30, validators=[RegexValidator("^[\S ]{6,}$")],
            help_text="最少6個字的字母", label="密碼",
            required=True, widget=forms.PasswordInput())
        password2 = forms.CharField(
            max_length=30, validators=[RegexValidator("^\w{6,}$")],
            help_text="請再輸入一次密碼", label="確認密碼",
            required=True, widget=forms.PasswordInput())

        def clean(self):
            cleaned_data = super(Form, self).clean()
            password = cleaned_data.get("password")
            password2 = cleaned_data.get("password2")
            if password != password2:
                raise forms.ValidationError("密碼不一致")
            username = cleaned_data.get("username")
            try:
                User.objects.get(username=username)
            except User.DoesNotExist:
                return cleaned_data
            else:
                raise forms.ValidationError("這個帳號已經有人使用過了")

    form = Form()
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            u = User(
                username=form.cleaned_data['username'],
                first_name=form.cleaned_data['name'])
            u.set_password(form.cleaned_data['password'])
            u.date_joined = datetime.now()
            u.save()
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'])
            login(request, user)
            return redirect(reverse('CopetForum.views.register.registerDone'))
    return render(request, 'CopetForum/authenticate/register.html',
                  {"form": form})


def registerDone(request):
    return render(request, 'CopetForum/authenticate/registerDone.html')
