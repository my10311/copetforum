from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.views import login_required
from django.shortcuts import render


@login_required
@user_passes_test(lambda u: u.is_superuser)
def management(request):
    return render(request, 'CopetForum/management/management.html')
