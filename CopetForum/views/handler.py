from django.shortcuts import render


def handle403(request):
    return render(request, 'CopetForum/errors/403.html')


def handle404(request):
    return render(request, 'CopetForum/errors/404.html')


def handle500(request):
    return render(request, 'CopetForum/errors/500.html')
